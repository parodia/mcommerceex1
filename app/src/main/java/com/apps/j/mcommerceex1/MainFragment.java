package com.apps.j.mcommerceex1;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by j on 14.04.15.
 */
public class MainFragment extends Fragment {

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        Button simpleButton = (Button) rootView.findViewById(R.id.button1);
        Button advancedButton = (Button) rootView.findViewById(R.id.button2);
        Button aboutButton = (Button) rootView.findViewById(R.id.button3);
        Button exitButton = (Button) rootView.findViewById(R.id.button4);

        simpleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, CalculatorFragment.newInstance("simple"));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        advancedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, CalculatorFragment.newInstance("advanced"));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, new AboutFragment());
                ft.addToBackStack(null);
                ft.commit();

            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return rootView;
    }
}