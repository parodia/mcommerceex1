package com.apps.j.mcommerceex1;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class CalculatorFragment extends Fragment {
    private String type;
    private String displayText = "";
    private float result = 0;

    public static CalculatorFragment newInstance(String type) {
        CalculatorFragment fragment = new CalculatorFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    public CalculatorFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_calculator, container, false);

        final TextView display = (TextView) view.findViewById(R.id.display);
        final TextView displayFinal = (TextView) view.findViewById(R.id.display2);

        //display.setText(displayText);

        view.findViewById(R.id.buttonBksp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (displayText.length() != 0) {
                    displayText = displayText.substring(0, displayText.length()-1);
                    display.setText(displayText);
                }
            }
        });
        Button buttonPlusMinus = (Button) view.findViewById(R.id.buttonPlusMinus);

        view.findViewById(R.id.buttonC).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayText = "";
                result = 0;
                display.setText("0");
                //displayFinal.setText("");
            }
        });

        List<Button> numbersButtons = new ArrayList<>();

        numbersButtons.add((Button) view.findViewById(R.id.button1));
        numbersButtons.add((Button) view.findViewById(R.id.button2));
        numbersButtons.add((Button) view.findViewById(R.id.button3));
        numbersButtons.add((Button) view.findViewById(R.id.button4));
        numbersButtons.add((Button) view.findViewById(R.id.button5));
        numbersButtons.add((Button) view.findViewById(R.id.button6));
        numbersButtons.add((Button) view.findViewById(R.id.button7));
        numbersButtons.add((Button) view.findViewById(R.id.button8));
        numbersButtons.add((Button) view.findViewById(R.id.button9));
        numbersButtons.add((Button) view.findViewById(R.id.button0));

        for (final Button button: numbersButtons) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayText = displayText.concat(button.getText().toString());
                    display.setText(displayText);
                }
            });
        }



        view.findViewById(R.id.buttonDot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (displayText.matches("\\d+(.*\\d+)?[/*\\- \\+^\\.]"))
                    if (!displayText.matches(".*\\d+[.]\\d+$"))
                        displayText = displayText.substring(0, displayText.length()-1) + ((Button)v).getText().toString();
                else
                    if (!displayText.matches("\\d+.*\\d+[.]\\d+$"))
                        displayText = displayText.concat(((Button)v).getText().toString());
                display.setText(displayText);
            }
        });


        List<Button> operationsButtons = new ArrayList<>();
        operationsButtons.add((Button)view.findViewById(R.id.buttonPlus));
        operationsButtons.add((Button)view.findViewById(R.id.buttonMinus));
        operationsButtons.add((Button)view.findViewById(R.id.buttonAsterix));
        operationsButtons.add((Button)view.findViewById(R.id.buttonSlash));

        for (final Button button: operationsButtons) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String sign = button.getText().toString();
                    if (displayText.matches("\\d+(.*\\d+)?")) {
                        displayText = displayText.concat(((Button) v).getText().toString());
                    } else if (displayText.matches("\\d+(.*\\d+)?[/*\\- \\+^\\.]")) {
                        //System.out.println(displayText);
                        if (displayText.substring(displayText.length()-1).compareTo(sign) == 0)
                            displayText = displayText.concat(((Button)v).getText().toString());
                        else
                            displayText = displayText.substring(0, displayText.length()-1) + ((Button)v).getText().toString();
                    } else

                        System.out.println(displayText+" doesnt match");

                    System.out.println(sign);
                    display.setText(displayText);
                }
            });
        }
/*
        view.findViewById(R.id.buttonAsterix).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (displayText.matches("\\d+.*\\d+[/*\\-+.]$"))
                    displayText = displayText.substring(0, displayText.length()-1) + ((Button)v).getText().toString();
                else
                    displayText = displayText.concat(((Button)v).getText().toString());
                display.setText(displayText);
            }
        });

*/
        view.findViewById(R.id.buttonEquals).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display.setText(parseResult(displayText));
                displayText = "";
            }
        });



        List<Button> advancedButtons = new ArrayList<>();

        Button buttonSin = (Button) view.findViewById(R.id.buttonSin);
        Button buttonCos = (Button) view.findViewById(R.id.buttonCos);
        Button buttonTan = (Button) view.findViewById(R.id.buttonTan);
        Button buttonLn = (Button) view.findViewById(R.id.buttonLn);
        Button buttonSqrt = (Button) view.findViewById(R.id.buttonSqrt);
        Button buttonX2 = (Button) view.findViewById(R.id.buttonX2);
        Button buttonXY = (Button) view.findViewById(R.id.buttonXY);
        Button buttonLog = (Button) view.findViewById(R.id.buttonLog);
        advancedButtons.add(buttonSin);
        advancedButtons.add(buttonCos);
        advancedButtons.add(buttonTan);
        advancedButtons.add(buttonLn);
        advancedButtons.add(buttonSqrt);
        advancedButtons.add(buttonX2);
        advancedButtons.add(buttonXY);
        advancedButtons.add(buttonLog);

        if (type.equals("simple")) {
            for (Button button: advancedButtons)
                button.setVisibility(View.GONE);
        }



        return view;
    }

    private String parseResult(String text) {
        System.out.println(text);

        Double result = ExpressionParser.calculate(text);
        int result2 = 0;
        try {
            result2 = result.intValue();
        } catch (Exception e) {
            System.out.println(e);
            
        }
        return String.valueOf(result);
    }


}
